﻿CREATE TABLE [dbo].[CadastroDeReceitas]
(
	[IdReceita] INT NOT NULL PRIMARY KEY IDENTITY,
	[NomeReceita] VARCHAR(255) NOT NULL,
	[CategoriaReceita] VARCHAR(255) NOT NULL,
	[ModoDePreparo] VARCHAR(255) NOT NULL,
	[Igredientes] VARCHAR(255) NOT NULL,
	[TempoDePreparo] TIME NOT NULL,
	[ValorReceita] FLOAT NOT NULL
)

CREATE TABLE [dbo].[CadastroDeLogin]
(
	[idLogin] INT NOT NULL AUTO_INCREMENT IDENTITY,
    [Login] VARCHAR(255) NOT NULL,
    [Senha] VARCHAR(255) NOT NULL
)

CREATE TABLE [dbo].[CadastroDeIngredientes]
(

)

