﻿CREATE TABLE [dbo].[Table]
(
	[IdReceita] INT NOT NULL PRIMARY KEY IDENTITY,
	[NomeReceita] VARCHAR(255) NOT NULL,
	[CategoriaReceita] VARCHAR(255) NOT NULL,
	[ModoDePreparo] VARCHAR(255) NOT NULL,
	[Igredientes] VARCHAR(255) NOT NULL,
	[TempoDePreparo] TIME NOT NULL,
	[ValorReceita] FLOAT NOT NULL

)
