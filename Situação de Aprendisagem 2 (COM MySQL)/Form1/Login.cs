﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        string[] login = new string[10];
        string[] senha = new string[10];

        int i = 0;

        private void Login_Load(object sender, EventArgs e)
        {
            lbnArea.Visible = false;
            lbnNewlogin.Visible = false;
            lbnNewsenha.Visible = false;
            btnAcancel.Visible = false;
            btnAok.Visible = false;
            txtClogin.Visible = false;
            txtCsenha.Visible = false;
            lbnNota.Visible = false;

        }

        private void btnCadastra_Click(object sender, EventArgs e)
        {
            lbnArea.Visible = true;
            lbnNewlogin.Visible = true;
            lbnNewsenha.Visible = true;
            btnAcancel.Visible = true;
            btnAok.Visible = true;
            txtClogin.Visible = true;
            txtCsenha.Visible = true;
            lbnNota.Visible = true;
        }

        private void btnAok_Click(object sender, EventArgs e)
        {
            if (txtClogin.Text != null || txtSenha.Text != null)
            {
                for (i = 0; i < 9; i++)
                {
                    if (login[i] == null)
                    {
                        login[i] = txtClogin.Text;
                        senha[i] = txtCsenha.Text;

                        txtClogin.Text = "";
                        txtCsenha.Text = "";

                        lbnArea.Visible = false;
                        lbnNewlogin.Visible = false;
                        lbnNewsenha.Visible = false;
                        btnAcancel.Visible = false;
                        btnAok.Visible = false;
                        txtClogin.Visible = false;
                        txtCsenha.Visible = false;
                        lbnNota.Visible = false;

                        MessageBox.Show("Usuario cadastrado com sucesso.","Aviso",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                        break;
                    }
                }
            }
            else 
            {
                MessageBox.Show("Preecha login e ou senha para continuar.","Aviso",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void btnAcancel_Click(object sender, EventArgs e)
        {
            lbnArea.Visible = false;
            lbnNewlogin.Visible = false;
            lbnNewsenha.Visible = false;
            btnAcancel.Visible = false;
            btnAok.Visible = false;
            txtClogin.Visible = false;
            txtCsenha.Visible = false;
            lbnNota.Visible = false;

            txtClogin.Text = "";
            txtCsenha.Text = "";

        }

        private void btnAcessar_Click(object sender, EventArgs e)
        {
            for (i = 0; i < 9; i++)
            {
                if (/*txtLogin.Text == login[i] && txtSenha.Text == senha[i]*/ txtLogin.Text == "a" && txtSenha.Text == "a")
                {
                    txtLogin.Text = "";
                    txtSenha.Text = "";
                    MenuPrincipal Tmenu = new MenuPrincipal(this);
                    Tmenu.Show();
                    this.Visible = false;                    

                    break;
                }
                else if (txtLogin.Text != login[9] || txtSenha.Text != senha[9])
                {
                    MessageBox.Show("Falha no login. Senha ou login incorreto.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtClogin_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
