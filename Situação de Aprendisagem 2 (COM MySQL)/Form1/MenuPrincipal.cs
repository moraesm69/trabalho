﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form1
{
    public partial class MenuPrincipal : Form
    {
        Login InstanciaDoLogin;

        public MenuPrincipal(Login PLogin)
        {
            InitializeComponent();

            InstanciaDoLogin = PLogin;
        }

        //Variaveis//
        int a = 0;

        //Vetores de Armazenamento//    
        string[] NomeReceita = new string[2];
        string[] PreparoReceita = new string[2];
        string[] TipoReceita = new string[2];

        string[] NomeIngrediente = new string[2];
        string[] Quant_Ingrediente = new string[2];

        private void MenuPrincipal_Load(object sender, EventArgs e) // Oculta os painel de busca e cadastro na exibição da tela.
        {
            pnlCadastro.Visible = false;
            pnlCotacao.Visible = false;
            pnlCadastroReceitas.Visible = false;
            pnlCadastroIngredientes.Visible = false;
            panelReceitasCadastradas.Visible = false;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            InstanciaDoLogin.Visible = true;
            Visible = false;
        }

        private void btnFormCadastro_Click(object sender, EventArgs e) // Evento para exibir ou oculta o painel de cadastro.
        {
            pnlCotacao.Visible = false;
            pnlCadastroIngredientes.Visible = false;
            pnlCadastroReceitas.Visible = false;
            panelReceitasCadastradas.Visible = false;            

            if (pnlCadastro.Visible == true)
            {
                a = 1;
                if (a == 1)
                {
                    pnlCadastro.Visible = false;
                }
            }
            else if (pnlCadastro.Visible == false)
            {
                pnlCadastro.Visible = true;

                a = 0;
            }
        }

        private void btnAjudaBusca_Click(object sender, EventArgs e)
        {
           
        }

        private void btnAjudaCadastro_Click(object sender, EventArgs e)
        {
            AjudaCadastroBusca TajudaCB = new AjudaCadastroBusca();
            TajudaCB.Show();
        }

        private void btnCadReceitas_Click(object sender, EventArgs e)
        {            
            pnlCadastroIngredientes.Visible = false;
            panelReceitasCadastradas.Visible = false;

            if (pnlCadastroReceitas.Visible == true)
             {
                 a = 1;
                 if (a == 1)
                 {
                     pnlCadastroReceitas.Visible = false;
                 }
             }
             else if (pnlCadastroReceitas.Visible == false)
             {
                 pnlCadastroReceitas.Visible = true;

                 a = 0;
             }            
        }    

        private void btnSair_Click(object sender, EventArgs e)
        {
            InstanciaDoLogin.Close();
        }
 
        private void btnPanelCadastRCancelar_Click_1(object sender, EventArgs e)
        {
            pnlCadastroReceitas.Visible = false;
        }

        private void btnPanelCadastRLimpar_Click(object sender, EventArgs e)
        {
            
            txtPanelCadastRNome.Text = "";
            txtPanelCadastRIngredientes.Text = "";
            txtPanelCadastRNome.Text = "";
            txtPanelCadastRPreparo.Text = "";
            txtPanelCadastRValor.Text = "";
            maskPanelCadastRTempo.Text = "";
            
        }

        private void btnCadIngredientes_Click(object sender, EventArgs e)
        {            
            pnlCadastroReceitas.Visible = false;
            panelReceitasCadastradas.Visible = false;

            if (pnlCadastroIngredientes.Visible == true)
            {
                a = 1;
                if (a == 1)
                {
                    pnlCadastroIngredientes.Visible = false;
                }
            }
            else if (pnlCadastroIngredientes.Visible == false)
            {
                pnlCadastroIngredientes.Visible = true;

                a = 0;
            }
        }

        private void pnlCadastroReceitas_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnBuscaReceitas_Click(object sender, EventArgs e)
        {
            pnlCadastroReceitas.Visible = false;
            pnlCadastroIngredientes.Visible = false;
            
            if (panelReceitasCadastradas.Visible == true)
            {
                a = 1;
                if (a == 1)
                {
                    panelReceitasCadastradas.Visible = false;
                }
            }
            else if (panelReceitasCadastradas.Visible == false)
            {
                panelReceitasCadastradas.Visible = true;

                a = 0;
            }
        }

        private void btnFormCotacao_Click_1(object sender, EventArgs e)
        {
            pnlCadastro.Visible = false;
            pnlCadastroReceitas.Visible = false;
            pnlCadastroIngredientes.Visible = false;
            panelReceitasCadastradas.Visible = false;
           

            if (pnlCotacao.Visible == true)
            {
                a = 1;
                if (a == 1)
                {
                    pnlCotacao.Visible = false;
                }
            }
            else if (pnlCotacao.Visible == false)
            {
                pnlCotacao.Visible = true;

                a = 0;
            }
        }
    }
}
