﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Form1
{
    class ClassReceitas
    {
        public int IdReceita { get; set; }
        public string NomeReceita { get; set; }
        public string CategoriaReceita { get; set; }
        public float ValorDePreparo { get; set; }
        public string ModoDePreparo { get; set; }
        public string IngredientesReceita { get; set; }


        ClassAcessBanco AcessBanco = new ClassAcessBanco();
    }
}
