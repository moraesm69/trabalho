﻿namespace Form1
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
            this.btnVoltar = new System.Windows.Forms.Button();
            this.lbnTitulo = new System.Windows.Forms.Label();
            this.btnFormCotacao = new System.Windows.Forms.Button();
            this.btnFormCadastro = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.pnlFormBotoes = new System.Windows.Forms.Panel();
            this.lbnPainelCadastro = new System.Windows.Forms.Label();
            this.btnCadReceitas = new System.Windows.Forms.Button();
            this.btnCadIngredientes = new System.Windows.Forms.Button();
            this.btnAjudaCadastro = new System.Windows.Forms.Button();
            this.lbnPainelBusca = new System.Windows.Forms.Label();
            this.btnBuscaReceitas = new System.Windows.Forms.Button();
            this.btnBuscaIngredientes = new System.Windows.Forms.Button();
            this.pnlCadastro = new System.Windows.Forms.Panel();
            this.lbnFormCadastro = new System.Windows.Forms.Label();
            this.lbnFormCotação = new System.Windows.Forms.Label();
            this.lbnFormSair = new System.Windows.Forms.Label();
            this.lbnPBuscas = new System.Windows.Forms.Label();
            this.btnCotaçãoReceitas = new System.Windows.Forms.Button();
            this.btnCotaçãoIngredientes = new System.Windows.Forms.Button();
            this.btnAjudaBusca = new System.Windows.Forms.Button();
            this.pnlCotacao = new System.Windows.Forms.Panel();
            this.lbnPanelCadastRTitulo = new System.Windows.Forms.Label();
            this.lbnPanelCadastRNome = new System.Windows.Forms.Label();
            this.txtPanelCadastRNome = new System.Windows.Forms.TextBox();
            this.txtPanelCadastRPreparo = new System.Windows.Forms.TextBox();
            this.lbnCadastroReceitaIngredientes = new System.Windows.Forms.Label();
            this.lbnPanelCadastRCategoria = new System.Windows.Forms.Label();
            this.lbnPanelCadastRTempPreparo = new System.Windows.Forms.Label();
            this.maskPanelCadastRTempo = new System.Windows.Forms.MaskedTextBox();
            this.lbnPanelCadastRCusto = new System.Windows.Forms.Label();
            this.txtPanelCadastRValor = new System.Windows.Forms.TextBox();
            this.lbnPanelCadastRMoeda = new System.Windows.Forms.Label();
            this.txtPanelCadastRIngredientes = new System.Windows.Forms.TextBox();
            this.lbnCadastraReceitaPreparo = new System.Windows.Forms.Label();
            this.btnSalvarNovaReceita = new System.Windows.Forms.Button();
            this.btnLimparNovaReceita = new System.Windows.Forms.Button();
            this.btnCancelarNovaReceita = new System.Windows.Forms.Button();
            this.pnlCadastroReceitas = new System.Windows.Forms.Panel();
            this.lbnPanelCadastINome = new System.Windows.Forms.Label();
            this.lbnPanelCadastITitulo = new System.Windows.Forms.Label();
            this.txtPanelCadastINome = new System.Windows.Forms.TextBox();
            this.lbnPanelCadastIValorUnit = new System.Windows.Forms.Label();
            this.txtPanelCadastIValorUnit = new System.Windows.Forms.TextBox();
            this.lbnPanelCadastITipo = new System.Windows.Forms.Label();
            this.btnPanelCadastICancelar = new System.Windows.Forms.Button();
            this.btnPanelCadastISalvar = new System.Windows.Forms.Button();
            this.btnPanelCadastILimpar = new System.Windows.Forms.Button();
            this.pnlCadastroIngredientes = new System.Windows.Forms.Panel();
            this.comboxCategoriaReceita = new System.Windows.Forms.ComboBox();
            this.panelReceitasCadastradas = new System.Windows.Forms.Panel();
            this.cbxTipoIngrediente = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pnlFormBotoes.SuspendLayout();
            this.pnlCadastro.SuspendLayout();
            this.pnlCotacao.SuspendLayout();
            this.pnlCadastroReceitas.SuspendLayout();
            this.pnlCadastroIngredientes.SuspendLayout();
            this.panelReceitasCadastradas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Transparent;
            this.btnVoltar.BackgroundImage = global::Form1.Properties.Resources.back_hand_drawn_arrow_outline;
            this.btnVoltar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnVoltar.FlatAppearance.BorderSize = 0;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Location = new System.Drawing.Point(0, 3);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(64, 49);
            this.btnVoltar.TabIndex = 0;
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // lbnTitulo
            // 
            this.lbnTitulo.AutoSize = true;
            this.lbnTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbnTitulo.Font = new System.Drawing.Font("Monotype Corsiva", 30F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnTitulo.Location = new System.Drawing.Point(218, 3);
            this.lbnTitulo.Name = "lbnTitulo";
            this.lbnTitulo.Size = new System.Drawing.Size(259, 49);
            this.lbnTitulo.TabIndex = 1;
            this.lbnTitulo.Text = "Receitas do Chef";
            // 
            // btnFormCotacao
            // 
            this.btnFormCotacao.BackColor = System.Drawing.Color.Transparent;
            this.btnFormCotacao.BackgroundImage = global::Form1.Properties.Resources.transparency;
            this.btnFormCotacao.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnFormCotacao.Location = new System.Drawing.Point(120, 3);
            this.btnFormCotacao.Name = "btnFormCotacao";
            this.btnFormCotacao.Size = new System.Drawing.Size(83, 44);
            this.btnFormCotacao.TabIndex = 2;
            this.btnFormCotacao.UseVisualStyleBackColor = false;
            this.btnFormCotacao.Click += new System.EventHandler(this.btnFormCotacao_Click_1);
            // 
            // btnFormCadastro
            // 
            this.btnFormCadastro.BackColor = System.Drawing.Color.Transparent;
            this.btnFormCadastro.BackgroundImage = global::Form1.Properties.Resources.clipboard;
            this.btnFormCadastro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnFormCadastro.Location = new System.Drawing.Point(12, 3);
            this.btnFormCadastro.Name = "btnFormCadastro";
            this.btnFormCadastro.Size = new System.Drawing.Size(83, 44);
            this.btnFormCadastro.TabIndex = 1;
            this.btnFormCadastro.UseVisualStyleBackColor = false;
            this.btnFormCadastro.Click += new System.EventHandler(this.btnFormCadastro_Click);
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.Color.Transparent;
            this.btnSair.BackgroundImage = global::Form1.Properties.Resources.round_delete_button;
            this.btnSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSair.Location = new System.Drawing.Point(644, 3);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(83, 44);
            this.btnSair.TabIndex = 3;
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // pnlFormBotoes
            // 
            this.pnlFormBotoes.BackColor = System.Drawing.Color.Transparent;
            this.pnlFormBotoes.Controls.Add(this.btnSair);
            this.pnlFormBotoes.Controls.Add(this.btnFormCadastro);
            this.pnlFormBotoes.Controls.Add(this.btnFormCotacao);
            this.pnlFormBotoes.Location = new System.Drawing.Point(0, 58);
            this.pnlFormBotoes.Name = "pnlFormBotoes";
            this.pnlFormBotoes.Size = new System.Drawing.Size(739, 50);
            this.pnlFormBotoes.TabIndex = 3;
            // 
            // lbnPainelCadastro
            // 
            this.lbnPainelCadastro.AutoSize = true;
            this.lbnPainelCadastro.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPainelCadastro.Location = new System.Drawing.Point(30, 8);
            this.lbnPainelCadastro.Name = "lbnPainelCadastro";
            this.lbnPainelCadastro.Size = new System.Drawing.Size(108, 33);
            this.lbnPainelCadastro.TabIndex = 0;
            this.lbnPainelCadastro.Text = "Cadastrar";
            // 
            // btnCadReceitas
            // 
            this.btnCadReceitas.Font = new System.Drawing.Font("Monotype Corsiva", 16F, System.Drawing.FontStyle.Italic);
            this.btnCadReceitas.Location = new System.Drawing.Point(3, 44);
            this.btnCadReceitas.Name = "btnCadReceitas";
            this.btnCadReceitas.Size = new System.Drawing.Size(178, 53);
            this.btnCadReceitas.TabIndex = 1;
            this.btnCadReceitas.Text = "Receitas";
            this.btnCadReceitas.UseVisualStyleBackColor = true;
            this.btnCadReceitas.Click += new System.EventHandler(this.btnCadReceitas_Click);
            // 
            // btnCadIngredientes
            // 
            this.btnCadIngredientes.Font = new System.Drawing.Font("Monotype Corsiva", 16.25F, System.Drawing.FontStyle.Italic);
            this.btnCadIngredientes.Location = new System.Drawing.Point(3, 103);
            this.btnCadIngredientes.Name = "btnCadIngredientes";
            this.btnCadIngredientes.Size = new System.Drawing.Size(178, 53);
            this.btnCadIngredientes.TabIndex = 2;
            this.btnCadIngredientes.Text = "Ingredientes";
            this.btnCadIngredientes.UseVisualStyleBackColor = true;
            this.btnCadIngredientes.Click += new System.EventHandler(this.btnCadIngredientes_Click);
            // 
            // btnAjudaCadastro
            // 
            this.btnAjudaCadastro.Font = new System.Drawing.Font("Monotype Corsiva", 16.25F, System.Drawing.FontStyle.Italic);
            this.btnAjudaCadastro.Location = new System.Drawing.Point(3, 340);
            this.btnAjudaCadastro.Name = "btnAjudaCadastro";
            this.btnAjudaCadastro.Size = new System.Drawing.Size(178, 53);
            this.btnAjudaCadastro.TabIndex = 3;
            this.btnAjudaCadastro.Text = "Ajuda";
            this.btnAjudaCadastro.UseVisualStyleBackColor = true;
            this.btnAjudaCadastro.Click += new System.EventHandler(this.btnAjudaCadastro_Click);
            // 
            // lbnPainelBusca
            // 
            this.lbnPainelBusca.AutoSize = true;
            this.lbnPainelBusca.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPainelBusca.Location = new System.Drawing.Point(45, 168);
            this.lbnPainelBusca.Name = "lbnPainelBusca";
            this.lbnPainelBusca.Size = new System.Drawing.Size(80, 33);
            this.lbnPainelBusca.TabIndex = 4;
            this.lbnPainelBusca.Text = "Buscar";
            // 
            // btnBuscaReceitas
            // 
            this.btnBuscaReceitas.Font = new System.Drawing.Font("Monotype Corsiva", 16F, System.Drawing.FontStyle.Italic);
            this.btnBuscaReceitas.Location = new System.Drawing.Point(3, 204);
            this.btnBuscaReceitas.Name = "btnBuscaReceitas";
            this.btnBuscaReceitas.Size = new System.Drawing.Size(178, 53);
            this.btnBuscaReceitas.TabIndex = 5;
            this.btnBuscaReceitas.Text = "Receitas";
            this.btnBuscaReceitas.UseVisualStyleBackColor = true;
            this.btnBuscaReceitas.Click += new System.EventHandler(this.btnBuscaReceitas_Click);
            // 
            // btnBuscaIngredientes
            // 
            this.btnBuscaIngredientes.Font = new System.Drawing.Font("Monotype Corsiva", 16.25F, System.Drawing.FontStyle.Italic);
            this.btnBuscaIngredientes.Location = new System.Drawing.Point(3, 263);
            this.btnBuscaIngredientes.Name = "btnBuscaIngredientes";
            this.btnBuscaIngredientes.Size = new System.Drawing.Size(178, 53);
            this.btnBuscaIngredientes.TabIndex = 6;
            this.btnBuscaIngredientes.Text = "Ingredientes";
            this.btnBuscaIngredientes.UseVisualStyleBackColor = true;
            // 
            // pnlCadastro
            // 
            this.pnlCadastro.BackColor = System.Drawing.Color.LightSkyBlue;
            this.pnlCadastro.Controls.Add(this.btnBuscaIngredientes);
            this.pnlCadastro.Controls.Add(this.btnBuscaReceitas);
            this.pnlCadastro.Controls.Add(this.lbnPainelBusca);
            this.pnlCadastro.Controls.Add(this.btnAjudaCadastro);
            this.pnlCadastro.Controls.Add(this.btnCadIngredientes);
            this.pnlCadastro.Controls.Add(this.btnCadReceitas);
            this.pnlCadastro.Controls.Add(this.lbnPainelCadastro);
            this.pnlCadastro.Location = new System.Drawing.Point(0, 160);
            this.pnlCadastro.Name = "pnlCadastro";
            this.pnlCadastro.Size = new System.Drawing.Size(184, 396);
            this.pnlCadastro.TabIndex = 4;
            // 
            // lbnFormCadastro
            // 
            this.lbnFormCadastro.AutoSize = true;
            this.lbnFormCadastro.BackColor = System.Drawing.Color.Transparent;
            this.lbnFormCadastro.Font = new System.Drawing.Font("Monotype Corsiva", 12.25F, System.Drawing.FontStyle.Italic);
            this.lbnFormCadastro.Location = new System.Drawing.Point(23, 111);
            this.lbnFormCadastro.Name = "lbnFormCadastro";
            this.lbnFormCadastro.Size = new System.Drawing.Size(82, 40);
            this.lbnFormCadastro.TabIndex = 5;
            this.lbnFormCadastro.Text = "Receitas e \r\nIngredientes";
            // 
            // lbnFormCotação
            // 
            this.lbnFormCotação.AutoSize = true;
            this.lbnFormCotação.BackColor = System.Drawing.Color.Transparent;
            this.lbnFormCotação.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnFormCotação.Location = new System.Drawing.Point(134, 111);
            this.lbnFormCotação.Name = "lbnFormCotação";
            this.lbnFormCotação.Size = new System.Drawing.Size(50, 18);
            this.lbnFormCotação.TabIndex = 6;
            this.lbnFormCotação.Text = "Cotação";
            // 
            // lbnFormSair
            // 
            this.lbnFormSair.AutoSize = true;
            this.lbnFormSair.BackColor = System.Drawing.Color.Transparent;
            this.lbnFormSair.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnFormSair.Location = new System.Drawing.Point(665, 111);
            this.lbnFormSair.Name = "lbnFormSair";
            this.lbnFormSair.Size = new System.Drawing.Size(30, 18);
            this.lbnFormSair.TabIndex = 8;
            this.lbnFormSair.Text = "Sair";
            // 
            // lbnPBuscas
            // 
            this.lbnPBuscas.AutoSize = true;
            this.lbnPBuscas.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPBuscas.Location = new System.Drawing.Point(47, 12);
            this.lbnPBuscas.Name = "lbnPBuscas";
            this.lbnPBuscas.Size = new System.Drawing.Size(91, 33);
            this.lbnPBuscas.TabIndex = 0;
            this.lbnPBuscas.Text = "Cotação";
            // 
            // btnCotaçãoReceitas
            // 
            this.btnCotaçãoReceitas.Font = new System.Drawing.Font("Monotype Corsiva", 16F, System.Drawing.FontStyle.Italic);
            this.btnCotaçãoReceitas.Location = new System.Drawing.Point(3, 57);
            this.btnCotaçãoReceitas.Name = "btnCotaçãoReceitas";
            this.btnCotaçãoReceitas.Size = new System.Drawing.Size(178, 53);
            this.btnCotaçãoReceitas.TabIndex = 1;
            this.btnCotaçãoReceitas.Text = "Receitas";
            this.btnCotaçãoReceitas.UseVisualStyleBackColor = true;
            // 
            // btnCotaçãoIngredientes
            // 
            this.btnCotaçãoIngredientes.Font = new System.Drawing.Font("Monotype Corsiva", 16.25F, System.Drawing.FontStyle.Italic);
            this.btnCotaçãoIngredientes.Location = new System.Drawing.Point(3, 116);
            this.btnCotaçãoIngredientes.Name = "btnCotaçãoIngredientes";
            this.btnCotaçãoIngredientes.Size = new System.Drawing.Size(178, 53);
            this.btnCotaçãoIngredientes.TabIndex = 2;
            this.btnCotaçãoIngredientes.Text = "Ingredientes";
            this.btnCotaçãoIngredientes.UseVisualStyleBackColor = true;
            // 
            // btnAjudaBusca
            // 
            this.btnAjudaBusca.Font = new System.Drawing.Font("Monotype Corsiva", 16.25F, System.Drawing.FontStyle.Italic);
            this.btnAjudaBusca.Location = new System.Drawing.Point(3, 340);
            this.btnAjudaBusca.Name = "btnAjudaBusca";
            this.btnAjudaBusca.Size = new System.Drawing.Size(178, 53);
            this.btnAjudaBusca.TabIndex = 3;
            this.btnAjudaBusca.Text = "Ajuda";
            this.btnAjudaBusca.UseVisualStyleBackColor = true;
            this.btnAjudaBusca.Click += new System.EventHandler(this.btnAjudaBusca_Click);
            // 
            // pnlCotacao
            // 
            this.pnlCotacao.BackColor = System.Drawing.Color.LightSkyBlue;
            this.pnlCotacao.Controls.Add(this.btnAjudaBusca);
            this.pnlCotacao.Controls.Add(this.btnCotaçãoIngredientes);
            this.pnlCotacao.Controls.Add(this.btnCotaçãoReceitas);
            this.pnlCotacao.Controls.Add(this.lbnPBuscas);
            this.pnlCotacao.Location = new System.Drawing.Point(0, 157);
            this.pnlCotacao.Name = "pnlCotacao";
            this.pnlCotacao.Size = new System.Drawing.Size(184, 399);
            this.pnlCotacao.TabIndex = 7;
            // 
            // lbnPanelCadastRTitulo
            // 
            this.lbnPanelCadastRTitulo.AutoSize = true;
            this.lbnPanelCadastRTitulo.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastRTitulo.Location = new System.Drawing.Point(125, 12);
            this.lbnPanelCadastRTitulo.Name = "lbnPanelCadastRTitulo";
            this.lbnPanelCadastRTitulo.Size = new System.Drawing.Size(235, 33);
            this.lbnPanelCadastRTitulo.TabIndex = 0;
            this.lbnPanelCadastRTitulo.Text = "Cadastra Nova Receita";
            // 
            // lbnPanelCadastRNome
            // 
            this.lbnPanelCadastRNome.AutoSize = true;
            this.lbnPanelCadastRNome.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastRNome.Location = new System.Drawing.Point(3, 59);
            this.lbnPanelCadastRNome.Name = "lbnPanelCadastRNome";
            this.lbnPanelCadastRNome.Size = new System.Drawing.Size(49, 18);
            this.lbnPanelCadastRNome.TabIndex = 1;
            this.lbnPanelCadastRNome.Text = "Nome: ";
            // 
            // txtPanelCadastRNome
            // 
            this.txtPanelCadastRNome.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.txtPanelCadastRNome.Location = new System.Drawing.Point(58, 59);
            this.txtPanelCadastRNome.Name = "txtPanelCadastRNome";
            this.txtPanelCadastRNome.Size = new System.Drawing.Size(196, 23);
            this.txtPanelCadastRNome.TabIndex = 2;
            // 
            // txtPanelCadastRPreparo
            // 
            this.txtPanelCadastRPreparo.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.txtPanelCadastRPreparo.Location = new System.Drawing.Point(50, 158);
            this.txtPanelCadastRPreparo.Multiline = true;
            this.txtPanelCadastRPreparo.Name = "txtPanelCadastRPreparo";
            this.txtPanelCadastRPreparo.Size = new System.Drawing.Size(204, 176);
            this.txtPanelCadastRPreparo.TabIndex = 3;
            // 
            // lbnCadastroReceitaIngredientes
            // 
            this.lbnCadastroReceitaIngredientes.AutoSize = true;
            this.lbnCadastroReceitaIngredientes.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnCadastroReceitaIngredientes.Location = new System.Drawing.Point(275, 137);
            this.lbnCadastroReceitaIngredientes.Name = "lbnCadastroReceitaIngredientes";
            this.lbnCadastroReceitaIngredientes.Size = new System.Drawing.Size(79, 18);
            this.lbnCadastroReceitaIngredientes.TabIndex = 5;
            this.lbnCadastroReceitaIngredientes.Text = "Ingredientes";
            // 
            // lbnPanelCadastRCategoria
            // 
            this.lbnPanelCadastRCategoria.AutoSize = true;
            this.lbnPanelCadastRCategoria.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lbnPanelCadastRCategoria.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastRCategoria.Location = new System.Drawing.Point(260, 59);
            this.lbnPanelCadastRCategoria.Name = "lbnPanelCadastRCategoria";
            this.lbnPanelCadastRCategoria.Size = new System.Drawing.Size(64, 18);
            this.lbnPanelCadastRCategoria.TabIndex = 6;
            this.lbnPanelCadastRCategoria.Text = "Categoria:";
            // 
            // lbnPanelCadastRTempPreparo
            // 
            this.lbnPanelCadastRTempPreparo.AutoSize = true;
            this.lbnPanelCadastRTempPreparo.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastRTempPreparo.Location = new System.Drawing.Point(3, 93);
            this.lbnPanelCadastRTempPreparo.Name = "lbnPanelCadastRTempPreparo";
            this.lbnPanelCadastRTempPreparo.Size = new System.Drawing.Size(118, 18);
            this.lbnPanelCadastRTempPreparo.TabIndex = 10;
            this.lbnPanelCadastRTempPreparo.Text = "Tempo de Preparo: ";
            // 
            // maskPanelCadastRTempo
            // 
            this.maskPanelCadastRTempo.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.maskPanelCadastRTempo.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.maskPanelCadastRTempo.Location = new System.Drawing.Point(127, 92);
            this.maskPanelCadastRTempo.Mask = "00:00:00";
            this.maskPanelCadastRTempo.Name = "maskPanelCadastRTempo";
            this.maskPanelCadastRTempo.Size = new System.Drawing.Size(61, 23);
            this.maskPanelCadastRTempo.TabIndex = 11;
            // 
            // lbnPanelCadastRCusto
            // 
            this.lbnPanelCadastRCusto.AutoSize = true;
            this.lbnPanelCadastRCusto.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastRCusto.Location = new System.Drawing.Point(194, 93);
            this.lbnPanelCadastRCusto.Name = "lbnPanelCadastRCusto";
            this.lbnPanelCadastRCusto.Size = new System.Drawing.Size(134, 18);
            this.lbnPanelCadastRCusto.TabIndex = 12;
            this.lbnPanelCadastRCusto.Text = "Custo de Preparo:  R$";
            // 
            // txtPanelCadastRValor
            // 
            this.txtPanelCadastRValor.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.txtPanelCadastRValor.Location = new System.Drawing.Point(334, 92);
            this.txtPanelCadastRValor.Name = "txtPanelCadastRValor";
            this.txtPanelCadastRValor.Size = new System.Drawing.Size(58, 23);
            this.txtPanelCadastRValor.TabIndex = 13;
            // 
            // lbnPanelCadastRMoeda
            // 
            this.lbnPanelCadastRMoeda.AutoSize = true;
            this.lbnPanelCadastRMoeda.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastRMoeda.Location = new System.Drawing.Point(398, 93);
            this.lbnPanelCadastRMoeda.Name = "lbnPanelCadastRMoeda";
            this.lbnPanelCadastRMoeda.Size = new System.Drawing.Size(34, 18);
            this.lbnPanelCadastRMoeda.TabIndex = 14;
            this.lbnPanelCadastRMoeda.Text = "Real";
            // 
            // txtPanelCadastRIngredientes
            // 
            this.txtPanelCadastRIngredientes.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.txtPanelCadastRIngredientes.Location = new System.Drawing.Point(278, 158);
            this.txtPanelCadastRIngredientes.Multiline = true;
            this.txtPanelCadastRIngredientes.Name = "txtPanelCadastRIngredientes";
            this.txtPanelCadastRIngredientes.Size = new System.Drawing.Size(217, 176);
            this.txtPanelCadastRIngredientes.TabIndex = 15;
            // 
            // lbnCadastraReceitaPreparo
            // 
            this.lbnCadastraReceitaPreparo.AutoSize = true;
            this.lbnCadastraReceitaPreparo.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnCadastraReceitaPreparo.Location = new System.Drawing.Point(47, 137);
            this.lbnCadastraReceitaPreparo.Name = "lbnCadastraReceitaPreparo";
            this.lbnCadastraReceitaPreparo.Size = new System.Drawing.Size(110, 18);
            this.lbnCadastraReceitaPreparo.TabIndex = 16;
            this.lbnCadastraReceitaPreparo.Text = "Modo de Preparo.";
            // 
            // btnSalvarNovaReceita
            // 
            this.btnSalvarNovaReceita.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.btnSalvarNovaReceita.Location = new System.Drawing.Point(50, 359);
            this.btnSalvarNovaReceita.Name = "btnSalvarNovaReceita";
            this.btnSalvarNovaReceita.Size = new System.Drawing.Size(75, 23);
            this.btnSalvarNovaReceita.TabIndex = 17;
            this.btnSalvarNovaReceita.Text = "Salvar";
            this.btnSalvarNovaReceita.UseVisualStyleBackColor = true;
            // 
            // btnLimparNovaReceita
            // 
            this.btnLimparNovaReceita.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.btnLimparNovaReceita.Location = new System.Drawing.Point(233, 357);
            this.btnLimparNovaReceita.Name = "btnLimparNovaReceita";
            this.btnLimparNovaReceita.Size = new System.Drawing.Size(75, 23);
            this.btnLimparNovaReceita.TabIndex = 19;
            this.btnLimparNovaReceita.Text = "Limpar";
            this.btnLimparNovaReceita.UseVisualStyleBackColor = true;
            // 
            // btnCancelarNovaReceita
            // 
            this.btnCancelarNovaReceita.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.btnCancelarNovaReceita.Location = new System.Drawing.Point(420, 359);
            this.btnCancelarNovaReceita.Name = "btnCancelarNovaReceita";
            this.btnCancelarNovaReceita.Size = new System.Drawing.Size(75, 23);
            this.btnCancelarNovaReceita.TabIndex = 21;
            this.btnCancelarNovaReceita.Text = "Cancelar";
            this.btnCancelarNovaReceita.UseVisualStyleBackColor = true;
            // 
            // pnlCadastroReceitas
            // 
            this.pnlCadastroReceitas.BackColor = System.Drawing.Color.LightSkyBlue;
            this.pnlCadastroReceitas.Controls.Add(this.comboxCategoriaReceita);
            this.pnlCadastroReceitas.Controls.Add(this.btnCancelarNovaReceita);
            this.pnlCadastroReceitas.Controls.Add(this.btnLimparNovaReceita);
            this.pnlCadastroReceitas.Controls.Add(this.btnSalvarNovaReceita);
            this.pnlCadastroReceitas.Controls.Add(this.lbnCadastraReceitaPreparo);
            this.pnlCadastroReceitas.Controls.Add(this.txtPanelCadastRIngredientes);
            this.pnlCadastroReceitas.Controls.Add(this.lbnPanelCadastRMoeda);
            this.pnlCadastroReceitas.Controls.Add(this.txtPanelCadastRValor);
            this.pnlCadastroReceitas.Controls.Add(this.lbnPanelCadastRCusto);
            this.pnlCadastroReceitas.Controls.Add(this.maskPanelCadastRTempo);
            this.pnlCadastroReceitas.Controls.Add(this.lbnPanelCadastRTempPreparo);
            this.pnlCadastroReceitas.Controls.Add(this.lbnPanelCadastRCategoria);
            this.pnlCadastroReceitas.Controls.Add(this.lbnCadastroReceitaIngredientes);
            this.pnlCadastroReceitas.Controls.Add(this.txtPanelCadastRPreparo);
            this.pnlCadastroReceitas.Controls.Add(this.txtPanelCadastRNome);
            this.pnlCadastroReceitas.Controls.Add(this.lbnPanelCadastRNome);
            this.pnlCadastroReceitas.Controls.Add(this.lbnPanelCadastRTitulo);
            this.pnlCadastroReceitas.Location = new System.Drawing.Point(200, 132);
            this.pnlCadastroReceitas.Name = "pnlCadastroReceitas";
            this.pnlCadastroReceitas.Size = new System.Drawing.Size(527, 410);
            this.pnlCadastroReceitas.TabIndex = 12;
            this.pnlCadastroReceitas.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlCadastroReceitas_Paint);
            // 
            // lbnPanelCadastINome
            // 
            this.lbnPanelCadastINome.AutoSize = true;
            this.lbnPanelCadastINome.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastINome.Location = new System.Drawing.Point(3, 56);
            this.lbnPanelCadastINome.Name = "lbnPanelCadastINome";
            this.lbnPanelCadastINome.Size = new System.Drawing.Size(49, 18);
            this.lbnPanelCadastINome.TabIndex = 2;
            this.lbnPanelCadastINome.Text = "Nome: ";
            // 
            // lbnPanelCadastITitulo
            // 
            this.lbnPanelCadastITitulo.AutoSize = true;
            this.lbnPanelCadastITitulo.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastITitulo.Location = new System.Drawing.Point(84, 11);
            this.lbnPanelCadastITitulo.Name = "lbnPanelCadastITitulo";
            this.lbnPanelCadastITitulo.Size = new System.Drawing.Size(273, 33);
            this.lbnPanelCadastITitulo.TabIndex = 3;
            this.lbnPanelCadastITitulo.Text = "Cadastra Novo Ingrediente";
            // 
            // txtPanelCadastINome
            // 
            this.txtPanelCadastINome.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.txtPanelCadastINome.Location = new System.Drawing.Point(58, 55);
            this.txtPanelCadastINome.Name = "txtPanelCadastINome";
            this.txtPanelCadastINome.Size = new System.Drawing.Size(231, 23);
            this.txtPanelCadastINome.TabIndex = 4;
            // 
            // lbnPanelCadastIValorUnit
            // 
            this.lbnPanelCadastIValorUnit.AutoSize = true;
            this.lbnPanelCadastIValorUnit.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastIValorUnit.Location = new System.Drawing.Point(3, 85);
            this.lbnPanelCadastIValorUnit.Name = "lbnPanelCadastIValorUnit";
            this.lbnPanelCadastIValorUnit.Size = new System.Drawing.Size(100, 18);
            this.lbnPanelCadastIValorUnit.TabIndex = 7;
            this.lbnPanelCadastIValorUnit.Text = "Valor Unitario: ";
            // 
            // txtPanelCadastIValorUnit
            // 
            this.txtPanelCadastIValorUnit.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.txtPanelCadastIValorUnit.Location = new System.Drawing.Point(106, 84);
            this.txtPanelCadastIValorUnit.Name = "txtPanelCadastIValorUnit";
            this.txtPanelCadastIValorUnit.Size = new System.Drawing.Size(183, 23);
            this.txtPanelCadastIValorUnit.TabIndex = 8;
            // 
            // lbnPanelCadastITipo
            // 
            this.lbnPanelCadastITipo.AutoSize = true;
            this.lbnPanelCadastITipo.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnPanelCadastITipo.Location = new System.Drawing.Point(312, 57);
            this.lbnPanelCadastITipo.Name = "lbnPanelCadastITipo";
            this.lbnPanelCadastITipo.Size = new System.Drawing.Size(41, 18);
            this.lbnPanelCadastITipo.TabIndex = 9;
            this.lbnPanelCadastITipo.Text = "Tipo: ";
            // 
            // btnPanelCadastICancelar
            // 
            this.btnPanelCadastICancelar.Font = new System.Drawing.Font("Monotype Corsiva", 10.75F, System.Drawing.FontStyle.Italic);
            this.btnPanelCadastICancelar.Location = new System.Drawing.Point(365, 165);
            this.btnPanelCadastICancelar.Name = "btnPanelCadastICancelar";
            this.btnPanelCadastICancelar.Size = new System.Drawing.Size(75, 23);
            this.btnPanelCadastICancelar.TabIndex = 11;
            this.btnPanelCadastICancelar.Text = "Cancelar";
            this.btnPanelCadastICancelar.UseVisualStyleBackColor = true;
            // 
            // btnPanelCadastISalvar
            // 
            this.btnPanelCadastISalvar.Font = new System.Drawing.Font("Monotype Corsiva", 10.75F, System.Drawing.FontStyle.Italic);
            this.btnPanelCadastISalvar.Location = new System.Drawing.Point(15, 165);
            this.btnPanelCadastISalvar.Name = "btnPanelCadastISalvar";
            this.btnPanelCadastISalvar.Size = new System.Drawing.Size(75, 23);
            this.btnPanelCadastISalvar.TabIndex = 13;
            this.btnPanelCadastISalvar.Text = "Salvar";
            this.btnPanelCadastISalvar.UseVisualStyleBackColor = true;
            // 
            // btnPanelCadastILimpar
            // 
            this.btnPanelCadastILimpar.Font = new System.Drawing.Font("Monotype Corsiva", 10.25F, System.Drawing.FontStyle.Italic);
            this.btnPanelCadastILimpar.Location = new System.Drawing.Point(198, 165);
            this.btnPanelCadastILimpar.Name = "btnPanelCadastILimpar";
            this.btnPanelCadastILimpar.Size = new System.Drawing.Size(75, 23);
            this.btnPanelCadastILimpar.TabIndex = 14;
            this.btnPanelCadastILimpar.Text = "Limpar";
            this.btnPanelCadastILimpar.UseVisualStyleBackColor = true;
            // 
            // pnlCadastroIngredientes
            // 
            this.pnlCadastroIngredientes.BackColor = System.Drawing.Color.LightSkyBlue;
            this.pnlCadastroIngredientes.Controls.Add(this.cbxTipoIngrediente);
            this.pnlCadastroIngredientes.Controls.Add(this.btnPanelCadastILimpar);
            this.pnlCadastroIngredientes.Controls.Add(this.btnPanelCadastISalvar);
            this.pnlCadastroIngredientes.Controls.Add(this.btnPanelCadastICancelar);
            this.pnlCadastroIngredientes.Controls.Add(this.lbnPanelCadastITipo);
            this.pnlCadastroIngredientes.Controls.Add(this.txtPanelCadastIValorUnit);
            this.pnlCadastroIngredientes.Controls.Add(this.lbnPanelCadastIValorUnit);
            this.pnlCadastroIngredientes.Controls.Add(this.txtPanelCadastINome);
            this.pnlCadastroIngredientes.Controls.Add(this.lbnPanelCadastITitulo);
            this.pnlCadastroIngredientes.Controls.Add(this.lbnPanelCadastINome);
            this.pnlCadastroIngredientes.Location = new System.Drawing.Point(233, 227);
            this.pnlCadastroIngredientes.Name = "pnlCadastroIngredientes";
            this.pnlCadastroIngredientes.Size = new System.Drawing.Size(478, 206);
            this.pnlCadastroIngredientes.TabIndex = 24;
            // 
            // comboxCategoriaReceita
            // 
            this.comboxCategoriaReceita.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboxCategoriaReceita.FormattingEnabled = true;
            this.comboxCategoriaReceita.Items.AddRange(new object[] {
            "Carnes",
            "Bolos",
            "Doces",
            "Massas",
            "Cereais",
            "Vegano"});
            this.comboxCategoriaReceita.Location = new System.Drawing.Point(330, 59);
            this.comboxCategoriaReceita.Name = "comboxCategoriaReceita";
            this.comboxCategoriaReceita.Size = new System.Drawing.Size(102, 21);
            this.comboxCategoriaReceita.TabIndex = 22;
            // 
            // panelReceitasCadastradas
            // 
            this.panelReceitasCadastradas.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panelReceitasCadastradas.Controls.Add(this.dataGridView1);
            this.panelReceitasCadastradas.Controls.Add(this.label1);
            this.panelReceitasCadastradas.Location = new System.Drawing.Point(200, 132);
            this.panelReceitasCadastradas.Name = "panelReceitasCadastradas";
            this.panelReceitasCadastradas.Size = new System.Drawing.Size(527, 411);
            this.panelReceitasCadastradas.TabIndex = 25;
            // 
            // cbxTipoIngrediente
            // 
            this.cbxTipoIngrediente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTipoIngrediente.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbxTipoIngrediente.FormattingEnabled = true;
            this.cbxTipoIngrediente.Items.AddRange(new object[] {
            "Carnes",
            "Peixes",
            "Massas",
            "Frutas",
            "Temperos",
            "Oleos",
            "Exoticos",
            "Outros"});
            this.cbxTipoIngrediente.Location = new System.Drawing.Point(359, 57);
            this.cbxTipoIngrediente.Name = "cbxTipoIngrediente";
            this.cbxTipoIngrediente.Size = new System.Drawing.Size(103, 21);
            this.cbxTipoIngrediente.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 21F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(145, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(227, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Receitas Cadastradas";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 121);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(518, 81);
            this.dataGridView1.TabIndex = 1;
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(739, 556);
            this.Controls.Add(this.lbnFormSair);
            this.Controls.Add(this.lbnFormCotação);
            this.Controls.Add(this.lbnFormCadastro);
            this.Controls.Add(this.pnlFormBotoes);
            this.Controls.Add(this.lbnTitulo);
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.pnlCotacao);
            this.Controls.Add(this.pnlCadastro);
            this.Controls.Add(this.pnlCadastroReceitas);
            this.Controls.Add(this.pnlCadastroIngredientes);
            this.Controls.Add(this.panelReceitasCadastradas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(233, 227);
            this.Name = "MenuPrincipal";
            this.Text = "MenuPrincipal";
            this.Load += new System.EventHandler(this.MenuPrincipal_Load);
            this.pnlFormBotoes.ResumeLayout(false);
            this.pnlCadastro.ResumeLayout(false);
            this.pnlCadastro.PerformLayout();
            this.pnlCotacao.ResumeLayout(false);
            this.pnlCotacao.PerformLayout();
            this.pnlCadastroReceitas.ResumeLayout(false);
            this.pnlCadastroReceitas.PerformLayout();
            this.pnlCadastroIngredientes.ResumeLayout(false);
            this.pnlCadastroIngredientes.PerformLayout();
            this.panelReceitasCadastradas.ResumeLayout(false);
            this.panelReceitasCadastradas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.Label lbnTitulo;
        private System.Windows.Forms.Button btnFormCotacao;
        private System.Windows.Forms.Button btnFormCadastro;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Panel pnlFormBotoes;
        private System.Windows.Forms.Label lbnPainelCadastro;
        private System.Windows.Forms.Button btnCadReceitas;
        private System.Windows.Forms.Button btnCadIngredientes;
        private System.Windows.Forms.Button btnAjudaCadastro;
        private System.Windows.Forms.Label lbnPainelBusca;
        private System.Windows.Forms.Button btnBuscaReceitas;
        private System.Windows.Forms.Button btnBuscaIngredientes;
        private System.Windows.Forms.Panel pnlCadastro;
        private System.Windows.Forms.Label lbnFormCadastro;
        private System.Windows.Forms.Label lbnFormCotação;
        private System.Windows.Forms.Label lbnFormSair;
        private System.Windows.Forms.Label lbnPBuscas;
        private System.Windows.Forms.Button btnCotaçãoReceitas;
        private System.Windows.Forms.Button btnCotaçãoIngredientes;
        private System.Windows.Forms.Button btnAjudaBusca;
        private System.Windows.Forms.Panel pnlCotacao;
        private System.Windows.Forms.Label lbnPanelCadastRTitulo;
        private System.Windows.Forms.Label lbnPanelCadastRNome;
        private System.Windows.Forms.Label lbnCadastroReceitaIngredientes;
        private System.Windows.Forms.Label lbnPanelCadastRCategoria;
        private System.Windows.Forms.Label lbnPanelCadastRTempPreparo;
        private System.Windows.Forms.Label lbnPanelCadastRCusto;
        private System.Windows.Forms.Label lbnPanelCadastRMoeda;
        private System.Windows.Forms.Label lbnCadastraReceitaPreparo;
        private System.Windows.Forms.Button btnSalvarNovaReceita;
        private System.Windows.Forms.Button btnLimparNovaReceita;
        private System.Windows.Forms.Button btnCancelarNovaReceita;
        private System.Windows.Forms.Panel pnlCadastroReceitas;
        private System.Windows.Forms.Label lbnPanelCadastINome;
        private System.Windows.Forms.Label lbnPanelCadastITitulo;
        private System.Windows.Forms.TextBox txtPanelCadastINome;
        private System.Windows.Forms.Label lbnPanelCadastIValorUnit;
        private System.Windows.Forms.TextBox txtPanelCadastIValorUnit;
        private System.Windows.Forms.Label lbnPanelCadastITipo;
        private System.Windows.Forms.Button btnPanelCadastICancelar;
        private System.Windows.Forms.Button btnPanelCadastISalvar;
        private System.Windows.Forms.Button btnPanelCadastILimpar;
        private System.Windows.Forms.Panel pnlCadastroIngredientes;
        private System.Windows.Forms.Panel panelReceitasCadastradas;
        private System.Windows.Forms.ComboBox cbxTipoIngrediente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.TextBox txtPanelCadastRNome;
        public System.Windows.Forms.TextBox txtPanelCadastRPreparo;
        public System.Windows.Forms.MaskedTextBox maskPanelCadastRTempo;
        public System.Windows.Forms.TextBox txtPanelCadastRValor;
        public System.Windows.Forms.TextBox txtPanelCadastRIngredientes;
        public System.Windows.Forms.ComboBox comboxCategoriaReceita;
    }
}