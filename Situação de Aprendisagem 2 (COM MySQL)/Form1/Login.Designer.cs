﻿namespace Form1
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.PictureBox ImgLogin;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.lbnLogin = new System.Windows.Forms.Label();
            this.lbnSenha = new System.Windows.Forms.Label();
            this.btnAcessar = new System.Windows.Forms.Button();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.btnCancela = new System.Windows.Forms.Button();
            this.lbnArea = new System.Windows.Forms.Label();
            this.txtClogin = new System.Windows.Forms.TextBox();
            this.btnCadastra = new System.Windows.Forms.Button();
            this.lbnNewlogin = new System.Windows.Forms.Label();
            this.btnAok = new System.Windows.Forms.Button();
            this.btnAcancel = new System.Windows.Forms.Button();
            this.lbnNewsenha = new System.Windows.Forms.Label();
            this.txtCsenha = new System.Windows.Forms.TextBox();
            this.lbnNota = new System.Windows.Forms.Label();
            ImgLogin = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(ImgLogin)).BeginInit();
            this.SuspendLayout();
            // 
            // ImgLogin
            // 
            ImgLogin.BackColor = System.Drawing.Color.Transparent;
            ImgLogin.BackgroundImage = global::Form1.Properties.Resources.chef_icon;
            ImgLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            ImgLogin.Location = new System.Drawing.Point(12, 12);
            ImgLogin.Name = "ImgLogin";
            ImgLogin.Size = new System.Drawing.Size(125, 130);
            ImgLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            ImgLogin.TabIndex = 5;
            ImgLogin.TabStop = false;
            // 
            // lbnLogin
            // 
            this.lbnLogin.AutoSize = true;
            this.lbnLogin.BackColor = System.Drawing.Color.Transparent;
            this.lbnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.lbnLogin.ForeColor = System.Drawing.Color.Black;
            this.lbnLogin.Location = new System.Drawing.Point(143, 12);
            this.lbnLogin.Name = "lbnLogin";
            this.lbnLogin.Size = new System.Drawing.Size(60, 20);
            this.lbnLogin.TabIndex = 0;
            this.lbnLogin.Text = "Login: ";
            // 
            // lbnSenha
            // 
            this.lbnSenha.AutoSize = true;
            this.lbnSenha.BackColor = System.Drawing.Color.Transparent;
            this.lbnSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.lbnSenha.Location = new System.Drawing.Point(143, 71);
            this.lbnSenha.Name = "lbnSenha";
            this.lbnSenha.Size = new System.Drawing.Size(66, 20);
            this.lbnSenha.TabIndex = 1;
            this.lbnSenha.Text = "Senha: ";
            // 
            // btnAcessar
            // 
            this.btnAcessar.BackColor = System.Drawing.Color.White;
            this.btnAcessar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAcessar.Location = new System.Drawing.Point(245, 134);
            this.btnAcessar.Name = "btnAcessar";
            this.btnAcessar.Size = new System.Drawing.Size(103, 36);
            this.btnAcessar.TabIndex = 2;
            this.btnAcessar.Text = "Acessar";
            this.btnAcessar.UseVisualStyleBackColor = false;
            this.btnAcessar.Click += new System.EventHandler(this.btnAcessar_Click);
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(147, 35);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(201, 20);
            this.txtLogin.TabIndex = 3;
            // 
            // txtSenha
            // 
            this.txtSenha.Location = new System.Drawing.Point(147, 94);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.PasswordChar = '*';
            this.txtSenha.Size = new System.Drawing.Size(201, 20);
            this.txtSenha.TabIndex = 4;
            // 
            // btnCancela
            // 
            this.btnCancela.BackColor = System.Drawing.Color.White;
            this.btnCancela.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancela.Location = new System.Drawing.Point(245, 370);
            this.btnCancela.Name = "btnCancela";
            this.btnCancela.Size = new System.Drawing.Size(103, 36);
            this.btnCancela.TabIndex = 6;
            this.btnCancela.Text = "Cancelar";
            this.btnCancela.UseVisualStyleBackColor = false;
            this.btnCancela.Click += new System.EventHandler(this.btnCancela_Click);
            // 
            // lbnArea
            // 
            this.lbnArea.AutoSize = true;
            this.lbnArea.BackColor = System.Drawing.Color.White;
            this.lbnArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbnArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnArea.ForeColor = System.Drawing.Color.Black;
            this.lbnArea.Location = new System.Drawing.Point(15, 178);
            this.lbnArea.Name = "lbnArea";
            this.lbnArea.Size = new System.Drawing.Size(152, 22);
            this.lbnArea.TabIndex = 7;
            this.lbnArea.Text = "Area de Cadastro";
            // 
            // txtClogin
            // 
            this.txtClogin.Location = new System.Drawing.Point(57, 213);
            this.txtClogin.Name = "txtClogin";
            this.txtClogin.Size = new System.Drawing.Size(105, 20);
            this.txtClogin.TabIndex = 8;
            this.txtClogin.TextChanged += new System.EventHandler(this.txtClogin_TextChanged);
            // 
            // btnCadastra
            // 
            this.btnCadastra.BackColor = System.Drawing.Color.White;
            this.btnCadastra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastra.Location = new System.Drawing.Point(15, 370);
            this.btnCadastra.Name = "btnCadastra";
            this.btnCadastra.Size = new System.Drawing.Size(103, 36);
            this.btnCadastra.TabIndex = 9;
            this.btnCadastra.Text = "Cadastrar";
            this.btnCadastra.UseVisualStyleBackColor = false;
            this.btnCadastra.Click += new System.EventHandler(this.btnCadastra_Click);
            // 
            // lbnNewlogin
            // 
            this.lbnNewlogin.AutoSize = true;
            this.lbnNewlogin.BackColor = System.Drawing.Color.Transparent;
            this.lbnNewlogin.Location = new System.Drawing.Point(12, 216);
            this.lbnNewlogin.Name = "lbnNewlogin";
            this.lbnNewlogin.Size = new System.Drawing.Size(39, 13);
            this.lbnNewlogin.TabIndex = 10;
            this.lbnNewlogin.Text = "Login: ";
            // 
            // btnAok
            // 
            this.btnAok.BackColor = System.Drawing.Color.Transparent;
            this.btnAok.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAok.BackgroundImage")));
            this.btnAok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAok.FlatAppearance.BorderSize = 0;
            this.btnAok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAok.Location = new System.Drawing.Point(57, 265);
            this.btnAok.Name = "btnAok";
            this.btnAok.Size = new System.Drawing.Size(35, 32);
            this.btnAok.TabIndex = 11;
            this.btnAok.UseVisualStyleBackColor = false;
            this.btnAok.Click += new System.EventHandler(this.btnAok_Click);
            // 
            // btnAcancel
            // 
            this.btnAcancel.BackColor = System.Drawing.Color.Transparent;
            this.btnAcancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAcancel.BackgroundImage")));
            this.btnAcancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAcancel.FlatAppearance.BorderSize = 0;
            this.btnAcancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAcancel.Location = new System.Drawing.Point(127, 265);
            this.btnAcancel.Name = "btnAcancel";
            this.btnAcancel.Size = new System.Drawing.Size(35, 32);
            this.btnAcancel.TabIndex = 12;
            this.btnAcancel.UseVisualStyleBackColor = false;
            this.btnAcancel.Click += new System.EventHandler(this.btnAcancel_Click);
            // 
            // lbnNewsenha
            // 
            this.lbnNewsenha.AutoSize = true;
            this.lbnNewsenha.BackColor = System.Drawing.Color.Transparent;
            this.lbnNewsenha.Location = new System.Drawing.Point(12, 242);
            this.lbnNewsenha.Name = "lbnNewsenha";
            this.lbnNewsenha.Size = new System.Drawing.Size(44, 13);
            this.lbnNewsenha.TabIndex = 14;
            this.lbnNewsenha.Text = "Senha: ";
            // 
            // txtCsenha
            // 
            this.txtCsenha.Location = new System.Drawing.Point(57, 239);
            this.txtCsenha.Name = "txtCsenha";
            this.txtCsenha.Size = new System.Drawing.Size(105, 20);
            this.txtCsenha.TabIndex = 13;
            // 
            // lbnNota
            // 
            this.lbnNota.AutoSize = true;
            this.lbnNota.BackColor = System.Drawing.Color.White;
            this.lbnNota.Location = new System.Drawing.Point(12, 311);
            this.lbnNota.Name = "lbnNota";
            this.lbnNota.Size = new System.Drawing.Size(235, 26);
            this.lbnNota.TabIndex = 15;
            this.lbnNota.Text = "Esta situação de aprendizagem foi desenvolvida\r\ncom atenção e carinho (^-^).\r\n";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(360, 420);
            this.Controls.Add(this.lbnNota);
            this.Controls.Add(this.lbnNewsenha);
            this.Controls.Add(this.txtCsenha);
            this.Controls.Add(this.btnAcancel);
            this.Controls.Add(this.btnAok);
            this.Controls.Add(this.lbnNewlogin);
            this.Controls.Add(this.btnCadastra);
            this.Controls.Add(this.txtClogin);
            this.Controls.Add(this.lbnArea);
            this.Controls.Add(this.btnCancela);
            this.Controls.Add(ImgLogin);
            this.Controls.Add(this.txtSenha);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.btnAcessar);
            this.Controls.Add(this.lbnSenha);
            this.Controls.Add(this.lbnLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Login";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Login_Load);
            ((System.ComponentModel.ISupportInitialize)(ImgLogin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbnLogin;
        private System.Windows.Forms.Label lbnSenha;
        private System.Windows.Forms.Button btnAcessar;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Button btnCancela;
        private System.Windows.Forms.Label lbnArea;
        private System.Windows.Forms.TextBox txtClogin;
        private System.Windows.Forms.Button btnCadastra;
        private System.Windows.Forms.Label lbnNewlogin;
        private System.Windows.Forms.Button btnAok;
        private System.Windows.Forms.Button btnAcancel;
        private System.Windows.Forms.Label lbnNewsenha;
        private System.Windows.Forms.TextBox txtCsenha;
        private System.Windows.Forms.Label lbnNota;
    }
}

